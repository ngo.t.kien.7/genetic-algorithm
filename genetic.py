import numpy as np
import random

class gene():
    def __init__(self):
        self.generate()
    def generate(self):
        self.pos1 = (random.randint(0,500),random.randint(0,500))
        self.pos2 = (random.randint(0,500),random.randint(0,500))
        self.pos3 = (random.randint(0,500),random.randint(0,500))
        self.color = {
            'r':random.randint(0,255),
            'g': random.randint(0,255),
            'b': random.randint(0,255),
            'a': 128
        }

class genetic():
    def crossover(self, parent1, parent2):
        switch = 1
        dna1 = parent1.getArray()
        dna2 = parent2.getArray()
        lastStep = 0
        currentStep = random.randint(lastStep,lastStep + random.choice([6,12,18,24,30,36,42,48]))
        dnaChild = []
        while lastStep <= len(dna1):
            if switch == 1:
                dnaChild.extend(dna1[lastStep:currentStep])
            else:
                dnaChild.extend(dna2[lastStep:currentStep])
            switch *= -1
            lastStep = currentStep
            currentStep = random.randint(lastStep,lastStep + random.choice([6,12,18,24,30,36,42,48]))
        return dnaChild
    
    def mutate(self, geneList, mutationRate):
        if random.uniform(0,1) > mutationRate:
            mutatedGene = random.sample(range(len(geneList)),5)
            for index in mutatedGene:
                geneList[index].generate()
        return geneList
