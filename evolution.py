from genetic import gene, genetic
import numpy as np
import random
import math
import operator
from PIL import Image, ImageDraw, ImageChops


background = 'white'
dimension = (500,500)
populationSize = 25
numTri = 250
generation = 200000
mutationRate = 0.1
childPerGen = 8
targetImage = './image/mastercard.png'
target = Image.open(targetImage).convert('RGB')
target = target.resize((500,500))
class individual():
    def __init__(self,parent1 = None, parent2 = None):
        self.child = False
        if parent1 and parent2:
            self.child = True
            crossGene = genetic()
            crossDna = crossGene.crossover(parent1, parent2)
            self.mutateFromParent(crossDna, mutationRate)
        else:
            self.gene = []
            for _ in range(numTri):
                self.gene.append(gene())
        self.background = background
        #Chưa có mutation from parents
        self.image = self.generateImage()
        self.fitness = self.getFitness(target)
    def generateImage(self):
        image = Image.new('RGB', dimension, background )
        draw = ImageDraw.Draw(image, 'RGBA')
        for gene in self.gene:
            draw.polygon([gene.pos1,gene.pos2,gene.pos3],
                        fill = (gene.color['r'],gene.color['g'],
                                gene.color['b'],gene.color['a']))
        del draw
        return image
    def saveImage(self, number):
        self.image.save('./output/' + str(number) + '.png','PNG')

    def getFitness(self, target):
        h = np.array(ImageChops.difference(target, self.image).histogram())
        weight = np.array([i for i in range(256)]*3)**2
        fitness = math.sqrt((h*weight).astype('int64').sum()/len(weight))
        return fitness
    def getArray(self):
        array = []
        for gene in self.gene:
            array.extend([gene.pos1,gene.pos2,gene.pos3,
                            gene.color['r'],gene.color['g'], gene.color['b'] ])
        return array

        
    def mutateFromParent(self, array, rate):
        self.gene = []
        for chunk in zip(*[iter(array)]*6):
            g = gene()
            if random.uniform(0,1)  > rate:
                g.pos1 = chunk[0]
                g.pos2 = chunk[1]
                g.pos3 = chunk[2]
                g.color['r'] = chunk[3]
                g.color['g'] = chunk[4]
                g.color['b'] = chunk[5]
            self.gene.append(g)
        


def initialize():
    pop = []
    for _ in range(populationSize):
        pop.append(individual(numTri))
    pop.sort(key = lambda x: x.getFitness(target))
    pop = pop[:populationSize]
    return pop

def evolve(pop):
    for genNum in range(generation):
        children = []
        parentChoice = []
        w = [i*10 for i in range(10,0,-1)]
        parentChoice = [(parent,weight) for parent, weight in zip(pop,w)]
        popChoice = [parent for parent,weight in parentChoice for _ in range(weight)]
        
        random.shuffle(popChoice)
        for _ in range(childPerGen):
            parent1 = random.choice(popChoice)
            parent2 = random.choice(popChoice)
            child = individual(parent1, parent2)
            children.append(child)
        pop += children
        pop.sort(key = lambda x: x.getFitness(target))
        pop = pop[:populationSize]
        print(pop[0].fitness)
        if genNum %10000 == 0:
            pop[0].saveImage(genNum)
        if genNum %100 == 0:
            print('generation: ', genNum, '| fitness: ', pop[0].fitness)
    return pop

if __name__ == '__main__':
    pop = initialize()
    pop = evolve(pop)
    pop[0].generateImage().show()    
