# PROJECT: 
Application of Genetic Algorithm on image generation

# DESCRIPTION:
This project is an application of the genetic algorithm to generate images from 250 triangles.

# METHODOLOGY:
## Genetic Algorithm ("GA"):
The algorithm is inspired by the process of evolution in the nature where species evolve/mutate to adapt to its surroundings.
GA is built from an initial population of which individuals are cross-breed in each new generation. Individuals with higher fitness
score are selected for breeding, while those with lower fitness score will be eliminated from the population. Using this method,
the genes that help increase the fitness score will be passed on to later generations.

## The Project:
Population: 25 individuals. Each individual consists of many genes represented by triangles.

Triangles: 250 triangles representing genes of each individual.

## Input:
A 500x500 image

## Output:
An image consisting of 250 triangles, replicating the Input image.

## SAMPLE OUTPUT:
### Gmail Logo

<img src="image/gmail.png" width = "200" height = "200">
<img src="sample/gmail0.png" width = "200" height = "200">
<img src="sample/gmail30000.png" width = "200" height = "200">
<img src="sample/gmail70000.png" width = "200" height = "200">

### Pepsi Logo
<img src="image/pepsi.jpg" width = "200" height = "200">
<img src="sample/pepsi0.png" width = "200" height = "200">
<img src="sample/pepsi10000.png" width = "200" height = "200">
<img src="sample/pepsi40000.png" width = "200" height = "200">









